#-------------------------------------------------------------------------------#
#                       Alpha_Blue To Black_And_White UI Palettes.
#-------------------------------------------------------------------------------#
#
# Replace Alpha_Blue Ground & Space Palettes with this one (file:ui_styles.inc)
#
#
#-------------------------------------------------------------------------------#
#                        ShivaShandra's Palettes Start
#-------------------------------------------------------------------------------#
			<Palette
				AccentDark='#2b2b2b'
				AccentLight='#6f6f6f'
				Activated='#727272'
				anim='#B90000'
				arrowActivated='#a7a7a7'
				arrowdefault='#a9a9a9'
				arrowDisabled='#000000'
				arrowHover='#FFFFFF'
				arrowSelected='#a9a9a9'
				back1='#111111'
				back2='#111111'
				back3='#111111'
				back4='#111111'
				back5='#111111'
				backDrop='#000000'
				BaseDark='#404040'
				BaseLight='#a7a7a7'
				bottomBar='#727272'
				box1='#848585'
				box2='#848585'
				box3='#848585'
				box4='#848585'
				box5='#848585'
				button1='#848585'
				button2='#848585'
				button3='#848585'
				button4='#848585'
				button5='#848585'
				carat='#FFFFFF'
				closeActivated='#DADADA'
				closeDarkActivated='#bababa'
				closeDarkHover='#F9F9F9'
				closeDarkNormal='#bababa'
				closeHover='#1E1E1E'
				closeNormal='#DADADA'
				contrast1='#FFFFFF'
				contrast2='#FFCE0D'
				contrast3='#00F4F4'
				contrast3a='#EEEEEE'
				contrast3h='#007D7D'
				contrast4='#EEEEEE'
				contrast4a='#EEEEEE'
				contrast5='#EEEEEE'
				contrast5a='#EEEEEE'
				default='#949494'
				disabled='#878787'
				disabledObject='#FF0000'
				EjectActivated='#ff0000'
				EjectDefault='#ff0000'
				EjectHover='#ffc000'
				equipped='#c1c1c1'
				exp='#F0F0F0'
				forceBackground='#727272'
				gamblingActivated='#FFA806'
				gamblingactivated2='#a5a5a5'
				gamblingAllBets='#EA0000'
				gamblingBack='#AE0000'
				gamblingCircle='#BF0000'
				gamblingDefault='#3c3c3c'
				gamblingdefault2='#a5a5a5'
				gamblingDisabled='#000000'
				gamblingDisabled2='#000000'
				gamblingFrame='#9a9a9a'
				gamblingGlow='#C60000'
				gamblingHover='#9a9a9a'
				gamblinghover2='#C61400'
				gamblingJackpot='#83FF06'
				gamblingLights='#FFA806'
				gamblingPlayerBet='#FFA806'
				gamblingPushBet='#FFFFFF'
				gamblingPushNumber='#292929'
				gamblingRouletteGlow='#E61700'
				gamblingRouletteText1='#000000'
				gamblingRouletteText2='#FEC007'
				glow='#c3c3c3'
				head1='#05B6B6'
				head2='#000000'
				head3='#000000'
				head4='#000000'
				head5='#000000'
				header='#BDC1C1'
				health='#FF0000'
				helpBox='#000000'
				Highlight='#b6b6b6'
				holocron='#a1a1a1'
				hover='#9f9f9f'
				Icon='#dbdbdb'
				iconactivated='#dbdbdb'
				icondefault='#dbdbdb'
				icondisabled='#858585'
				iconGlow='#ffffff'
				iconhover='#a9a9a9'
				IconNegative='#FF3333'
				IconPositive='#5CFF2D'
				jedicreate_holocron='#E71212'
				jedicreate_lightning='#a1a1a1'
				jedicreate_lightning2='#9a9a9a'
				lightning='#f2f2f2'
				line1='#b8b8b8'
				line2='#bababa'
				line3='#c3c3c3'
				line4='#c3c3c3'
				line5='#c3c3c3'
				listback='#606060'
				listHilite='#8d8d8d'
				listline='#FFFFFF'
				listtext='#eeeeee'
				loadBack='#7c7c7c'
				loadBar='#242424'
				loadbarFill='#A60000'
				mind='#0000FA'
				Name='Alpha_Blue'
				notcolor_ZoneAsteroid1='111'
				notcolor_ZoneAsteroid2='111'
				notcolor_ZoneDefault1='111'
				notcolor_ZoneDefault2='111'
				notcolor_ZoneHyperspace1='111'
				notcolor_ZoneHyperspace2='111'
				notcolor_ZoneMissionCritical1='111'
				notcolor_ZoneMissionCritical2='111'
				notcolor_ZoneSelect1='111'
				notcolor_ZoneSelect2='111'
				notcolor_ZoneShip1='111'
				notcolor_ZoneShip2='111'
				notcolor_ZoneSpaceStation1='111'
				notcolor_ZoneSpaceStation2='111'
				notcolor_ZoneWapointPoiActive2='111'
				notcolor_ZoneWaypointActive1='111'
				notcolor_ZoneWaypointActive2='111'
				notcolor_ZoneWaypointInactive1='111'
				notcolor_ZoneWaypointInactive2='111'
				notcolor_ZoneWaypointPoiActive1='111'
				notcolor_ZoneWaypointPoiInactive1='111'
				notcolor_ZoneWaypointPoiInactive2='111'
				numActivated='#801515'
				numDefault='#801515'
				numHover='#727272'
				numTextActivated='#D1D1D1'
				numTextDefault='#D1D1D1'
				numTextHover='#D1D1D1'
				outline1='#d6d6d6'
				outline2='#d6d6d6'
				outline3='#d6d6d6'
				outline4='#d6d6d6'
				outline5='#d6d6d6'
				OverlayDark='#818181'
				overlayLight='#e3e3e3'
				radactivated='#878787'
				radar='#949494'
				raddefault='#6f6f6f'
				raddisabled='#a7a7a7'
				radhover='#878787'
				radialTextDark='#ececec'
				radialTextLight='#ececec'
				radspin='#a1a1a1'
				Runner='#cccccc'
				runnerDark='#ffffff'
				scrollactivated='#FFAB0F'
				scrolldefault='#969696'
				scrolldisabled='#404040'
				scrollhover='#C8CCCC'
				scrollselected='#B7B9B9'
				selected='#790000'
				selectionBack='#000000'
				skillAcquiredActivated='#DDA600'
				skillAcquiredDefault='#DDA600'
				skillAcquiredDisabled='#DDA600'
				skillAcquiredHover='#B36700'
				skillAcquiredSelected='#DDA600'
				skillAcquiredTextActivated='#f4f4f4'
				skillAcquiredTextDefault='#f2f2f2'
				skillAcquiredTextHover='#f4f4f4'
				skillactivated='#9a9a9a'
				skilldefault='#9a9a9a'
				skilldisabled='#a9a9a9'
				skillhover='#666767'
				skillselected='#a9a9a9'
				skillTextActivated='#ffffff'
				skillTextDefault='#ffffff'
				slot='#a5a5a5'
				spaceGlow='#ffffff'
				spaceLoading='#ffffff'
				stamina='#00FA00'
				tab1='#929292'
				tab2='#929292'
				tab3='#929292'
				tab4='#929292'
				tab5='#929292'
				tableHeaderTextActivated='#e1e1e1'
				tableHeaderTextDefault='#cccccc'
				tableHeaderTextDisabled='#b4b4b4'
				tableHeaderTextHover='#000000'
				tableHeaderTextSelected='#e1e1e1'
				text1='#f2f2f2'
				text2='#f2f2f2'
				text3='#f2f2f2'
				text4='#f2f2f2'
				text5='#f2f2f2'
				textActivated='#E0E0E0'
				TextDark='#515151'
				textdefault='#DADCDC'
				textDisabled='#b4b4b4'
				textHover='#000000'
				textinsured='#F47A00'
				TextLight='#9c9c9c'
				textMagic='#A5F400'
				textMed='#6d6d6d'
				textSelected='#EAEAEA'
				textServer='#ffffff'
				textskill='#BBE903'
				titlebar='#727272'
				titleText='#d6d6d6'
				triangleHover='#D1D1D1'
				triangleOff='#949494'
				triangleOn='#F8F8F8'
				zoneback='#ffffff'
			/>
#-------------------------------------------------------------------------------#
#                        ShivaShandra's Palettes End
#-------------------------------------------------------------------------------#